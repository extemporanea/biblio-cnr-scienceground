# Biblio CNR  Scienceground

Percorsi bibliografici comuni Scienceground-CNR.

[[_TOC_]]

## Come partecipare?

Il modo più semplice di partecipare alla Biblioteca è di unirsi al gruppo su Zotero

https://www.zotero.org/groups/2536502/scienceground-cnr/

Si accede al gruppo in due passi:
1. creazione di un account su Zotero https://www.zotero.org/user/register
2. ricerca e aggiunta della libreria condivisa (*shared library*) Scienceground. Per poter modificare i contenuti, bisogna richiere l'accesso (e attendere la risposta). Per più informazioni sui gruppi in Zotero, vedere qui https://www.zotero.org/support/groups

Il modo più comodo di interagire con la libreria è di utilizzare il client. Questo può essere scaricato da qui https://www.zotero.org/download/

Nel client di Zotero, la libreria di gruppo si presenta così:

![interfaccia](fig/interface.png)


Per la costruzioni di percorsi, è sufficiente editare il file Markdown corrispondente https://gitlab.com/extemporanea1/biblio-cnr-scienceground/-/blob/master/percorsi/manuali/Percorsi%20bibliografici.md



## Come modificare la libreria?

Il modo più semplice per contribuire è tramite due possibili azioni

1. L'aggiunta di un titolo interessante
2. L'assegnazione di parole chiave (**tag**) ai titoli esistenti.

### Aggiunta di un titolo

Il metodo raccomandato per aggiungere un titolo è tramite l'ISBN o l'ISSN:

1. Individuare il codice ISBN del volume di riferimento
![commedia](fig/commedia.png)
2. Cliccare sull'icona con una penna magica e un segno + e inserire il codice isbn
![isbn](fig/isbn.png)


### Tag

Le parole chiave in Zotero sono chiamate *tag*. Ce ne sono di due tipi
1. tag assegnati **automaticamente** al momento dell'aggiunta automatica di un titolo
2. tag assegnati **manualmente**.

`Importante` *La costruzione dei percorsi bibliografici è basata sui tag assegnati manualmente.*

#### Tag automatici
Per escludere/includere i tag automatici, cliccare sul pulsante colorato nella parte bassa del riquadro dei tag

![tagmenu](fig/tagmenu.png)

#### Aggiunta tag

Per aggiungere un tag ad un titolo, cliccare in alto a destra sul pulsante tag e in seguito su **Add**

![tagmenu](fig/addtag.png)

Per assegnare lo stesso tag a più titoli, selezionare i titoli nel pannello centrale e trascinare il contenuto sul tag desiderato in basso a sinistra.

![tagmenu](fig/drag.gif)

## Percorsi bibliografici

Per la costruzioni di percorsi, è sufficiente editare il file Markdown corrispondente https://gitlab.com/extemporanea1/biblio-cnr-scienceground/-/blob/master/percorsi/manuali/Percorsi%20bibliografici.md

Un **percorso** è un insieme di titoli sotto la forma di una lista (senza ordine particolare). Ogni libro è introdotto dal suo titolo in _corsivo_ e da una breve spiegazione della sua rilevanza. Ad esempio

  - _La Scienza Che Fu_, di F Barreca,  un saggio sui destini delle idee seducenti (ma abbandonate) che hanno costellato il cammino della Fisica e della Chimica, come l'etere, il flogisto o gli atomi-vortici. Idee che han fatto da guida, talora portando su false piste, talaltra venendo riscoperte sotto nuova luce.

##### Note
1. **I percorsi non sono esaustivi:** un titolo può essere aggiunto alla libreria **senza** che faccia parte di alcun percorso.
2. **I percorsi non sono esclusivi:** un titolo può far parte di più di un percorso.

<!-- Una bozza di percorsi bibliografici può essere letta (e emendata) qui https://demo.codimd.org/s/H1nBhBMLP# -->
