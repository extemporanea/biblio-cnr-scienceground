# Usi e abusi della statistica
% Mark Twain attribuiva (erroneamente) al primo ministro inglese Disraeli il motto “Lies, damned lies, and statistics.” L’uso distorto della statistica è frequente nella vita di tutti i giorni, nei mass-media, nella stampa. Tuttavia anche la ricerca scientifica è soggetta a distorsioni spesso implicite e non intenzionali, che possono alterare significativamente il valore conoscitivo di una ricerca o, addirittura, di un intero campo di ricerca.
- Spurious Correlations
- Reckoning with Risk
- The Visual Display of Quantitative Information
- Crime Statistics in the News
- Statistics Done Wrong
- Bad Science
- How to Lie with Statistics
- Gravity's Ghost and Big Dog: scientific discovery and social analysis in the twenty-first century
- The Manga Guide to Statistics

# Storia e storie
% In questo percorso raccogliamo testi che affrontano la prospettiva storica del fare scienza. Ritroviamo qui saggi che tracciano i cammini di idee accantonate, diari personali, biografie entusiaste, approcci comparativi fra i problemi del presente e quelli degli antichi. La raccolta è un viaggio nella scienza del passato, con un occhio alla scienza del futuro.
- Che cos'è la scienza
- La Scienza Che Fu
- Viaggio di un professore tedesco all'Eldorado
- L'invenzione della natura 
- Storia Popolare della Scienza 
- Il dottor Semmelweis
- The Scientific Method
- Deadly Companions
- Environmental Problems of the Greeks and Romans
- The Dialectical Biologist  
- A Thousand Years of Nonlinear History
- Leviathan and the Air-Pump

#Scienza, Guerra, Società
%Questo percorso, sviluppato a partire da una serie di incontri organizzati nel 2020 in collaborazione con il gruppo ADI a Palazzo Ducale a Genova, nasce dall’esigenza da un lato di discutere il sempre maggiore coinvolgimento della scienza e della ricerca nello sviluppo di applicazioni militari; dall’altro di analizzare questo legame tra scienza e guerra anche da un punto di vista epistemologico, a partire dall’idea di controllo e sfruttamento della natura implicita in molta della ricerca scientifica.

- Teoria del drone: principi filosofici del diritto di uccidere
- War against the people
- War in the age of Intelligent Machines
- Che cos'è reale?
- Cuba: medicina, scienza e rivoluzione