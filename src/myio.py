
def read_library(items):
    library ={}
    for i,item in enumerate(items):
        data = item['data']
        title = data['title']
        # print(list(data.keys()))
        catalog = data['libraryCatalog']
        abstract = data['abstractNote']

        # print(title)
        tags = data['tags']
        # print(tags)
        if len(tags)==0:
            print(" !!! Warning: No tags for", title)
            continue

        # check that there are manual tags
        mtags = []
        for tag in tags:
            if 'type' in tag.keys():
                continue
            else:
                mtags.append(tag['tag'])

        if len(mtags)>0:
            library[title] = {}
            library[title]['tags'] =mtags

            for tag in mtags:
                creators = data['creators']
                # print (creators)
                authors = ''
                for  cr in creators:
                    # print(cr)
                    try:
                        authors+=cr['firstName']+' '+cr['lastName']+', '
                    except KeyError:
                        names=cr['name'].split(',')
                        # print(names)
                        try:
                            authors +=names[1]+' '+names[0]+', '
                        except IndexError:
                            authors = names[0]+", "



                library[title]['authors']=authors

                try:
                    library[title]['ISBN'] =data['ISBN']
                except:
                    library[title]['ISBN'] = data['ISSN']

                library[title]['catalog'] = catalog

                library[title]['abstract'] = '"'+abstract+'"'

    return library


def create_html_block(pair,intersection, lib):

    title = pair[0]+" and "+pair[1]
    block=f"""<div >

    <h2> {title}</h1>

    <ul>
    """
    for k in intersection:
        authors = lib[k]['authors']
        isbn = lib[k]['ISBN']
        block+=f'<li>{authors} <em>{k}</em>, ISBN/ISSN: {isbn}</li>'


    block+="</ul> </div>\n"
    return block

def create_markdown_block(pair,intersection, lib):

    title = pair[0]+" and "+pair[1]
    block=f"## {title}\n"


    for k in intersection:
        authors = lib[k]['authors']
        isbn = lib[k]['ISBN']
        block+=f'- {authors} **{k}**, ISBN/ISSN: {isbn}\n'


    block+="\n\n"
    return block
