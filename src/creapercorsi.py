import pandas as pd
from pyzotero import zotero
import numpy as np
import collections
import itertools
import myio


library_id = '2536502'

api_key = '448IcF6k31mfLAEa7ywdsqaC'

library_type = 'group'

zot = zotero.Zotero(library_id, library_type, api_key)
# print(inspect(zot.items))
items = zot.everything(zot.top())
# items = zot.top(limit=5)
# we've retrieved the latest five top-level items in our library
# we can print each item's item type and ID
# print(biblio['Titolo'])
print("The number of elements is", zot.count_items(), "and the list length is", len(items))

library = myio.read_library(items)


# print(library)
all_tags_with_repetition = np.concatenate([value['tags'] for key,value in library.items() ])
# print(all_tags)
unique_tags = np.unique(all_tags_with_repetition)
sets = {}
for u in unique_tags:
	sets[u] = set()

print ("::: The number of unique tags is", len(unique_tags))
for key,value in library.items():
	tags = value['tags']
	for tag in tags:
		sets[tag].add(key)


counter = collections.Counter(all_tags_with_repetition)
# print(counter)
# Pairs
combinations = itertools.combinations(unique_tags,2)

connections = {}
blockhtml =''
blockmd = ''
for c in combinations:
	intersection = sets[c[0]].intersection(sets[c[1]])
	cardinality = len(intersection)
	connections[c] = [cardinality, intersection]

	if cardinality>2:
		blockhtml += myio.create_html_block(c,intersection,library)
		blockmd += myio.create_markdown_block(c,intersection,library)


pagehtml =f"""<!doctype html>
			<html>
			<head>
			<meta charset="utf-8" />
			<link rel="stylesheet" href="../static/style.css">
			<title>Biblioteca CNRS - Scienceground </title>

			</head>

			<body>
			<h1> Biblioteca CNRS - Scienceground</h1>

			{blockhtml}

			</body>
			</html>

			"""
pagemd = f"""# Biblioteca CNRS - Scienceground

{blockmd}

"""

#write html
with open("../percorsi/automatici/html/index.html", 'w') as fw:
	fw.write(pagehtml)
with open("../percorsi/automatici/markdown/index.md", 'w') as fw:
	fw.write(pagemd)