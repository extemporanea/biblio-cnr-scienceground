import networkx as nx
from bokeh.io import show, output_file, save, export_svg
from bokeh.models import (
    Plot,
    Range1d,
    MultiLine,
    Circle,
    TapTool,
    OpenURL,
    HoverTool,
    CustomJS,
    Slider,
    Column,
    ColumnDataSource,
    WheelZoomTool,
    ResetTool,
    Text,
    Button,
    TabPanel,
    Tabs,
    CheckboxButtonGroup,
    MultiChoice,
)

from bokeh.models import Select, TextInput
from bokeh.layouts import column, grid
from bokeh.models import Div
from bokeh.models.graphs import EdgesAndLinkedNodes
from bokeh.plotting import figure, from_networkx
from bokeh.models.widgets import CheckboxGroup
import pandas as pd
from bokeh.layouts import row
import numpy as np
import matplotlib.pyplot as pl
from matplotlib import colors as mcolors
from collections import Counter
import matplotlib
import inpercorsi
from pathlib import Path

# ================= Modify the template to center the page ====================
template = """
{% block postamble %}
<style>

.bk-root {
width: 80%;
margin: 0 auto;
}

.bk-logo {
    display:none !important;
}


</style>
{% endblock %}
"""


# ============= Settings and read input =================
important = 100.0  # for the percorsi
standard = 0.1

# first run links.py to produce the CSV datasets from the Zotero library
taglinks = pd.read_csv("data/links.csv", sep="|")
nodes = pd.read_csv("data/nodes.csv", sep="|")
nodes.fillna("--", inplace=True)
# print(nodes)
checknames = list(np.unique(taglinks["edge_name"]))
idx = [checknames.index(s) for s in taglinks["edge_name"]]
taglinks["checkbox"] = idx

# read in the percorsi
read_percorsi = inpercorsi.Reader("data/percorsi.md")
percorsi = read_percorsi.percorsi
nodes["comment"] = "--"
# read all comments
for name, percorso in percorsi.items():
    for i, book in enumerate(percorso["list"]):
        pattern = "(?i)" + book["title"]
        nodes.loc[nodes["title"].str.contains(pattern), "comment"] = book["comment"]


# setup networkx graph
net_graph = nx.MultiGraph()

# ================================ constructing categories

# find most frequent edge name for node
node_category = {}
node_categories = {}
distinct_cats = {}
for v in nodes["id"]:
    try:
        names = taglinks[taglinks["idsource"] == v]["edge_name"]
        counter = Counter(names)
        distinct_cats[v] = len(list(counter.keys()))

        node_category[v] = np.random.choice(
            list(counter.keys()),
            p=np.array(list(counter.values())) / sum(counter.values()),
        )

        node_categories[v] = ", ".join(list(counter.keys()))
    except Exception as E:
        print("EXCEPTION in the construction of most frequent edges", E)
        distinct_cats[v] = 0
        node_category[v] = "--"
        node_categories[v] = "--"

unique_cat = np.unique(list(node_category.values()))

catcolors = {}

for i, c in enumerate(unique_cat):
    if i % 2 == 0:
        catcolors[c] = matplotlib.colors.rgb2hex(
            pl.cm.gist_ncar(i / int(1.05 * len(unique_cat)))
        )
    else:
        catcolors[c] = matplotlib.colors.rgb2hex(
            pl.cm.cubehelix_r(i / int(1.1 * len(unique_cat)))
        )  # avoid toobright range

# add all nodes
for _index, _row in nodes.iterrows():
    cats = node_categories[_row["id"]]
    cat = node_category[_row["id"]]
    catcolor = catcolors[cat]
    net_graph.add_node(
        _row["id"],
        title=_row["title"],
        author=_row["author"],
        isbn=_row["isbn"],
        category=cats,
        color=catcolor,
        comment=_row["comment"],
        catalog=_row["catalog"],
    )


# ============= constructing "percorsi"

u = 0
pathcolor = ["#ff7d78", "#00fa92", "#7980ff", "#0096ff", "#b076b4"]
for name, percorso in percorsi.items():
    percorsi[name]["color"] = pathcolor[u]
    for i, book in enumerate(percorso["list"]):
        # print(book)
        print("title", book["title"])
        # pattern ="(?i)Viaggio di un professore tedesco all’Eldorado" #"(?i)"+
        pattern = "(?i)" + book["title"]
        # print(pattern)
        values = nodes[nodes["title"].str.contains(pattern)]["id"].values
        # print(values)
        book["id"] = values[0]
        # print("after",book)
        end = book["id"]
        if i == 0:
            start = book["id"]

        else:
            net_graph.add_edge(
                start,
                end,
                checkbox=-1,
                edge_color=pathcolor[u],
                edge_width=0.0001,
                myweight=important,
                percorso=name,
                line_alpha=0.7,
                line_cap="round",
                line_join="round",
            )
            start = book["id"]

    u += 1


c = [
    "black",
    "#ff9300",
    "#919191",
    "#941651",
    "#8df900",
    "#521b92",
    "#005392",
    "#008e00",
    "#941100",
    "#ff2f92",
    "#9437ff",
    "#0096ff",
    "#ff7d78",
    "#7980ff",
    "#72fcd5",
    "black",
    "#ff9300",
    "#919191",
    "#941651",
    "#8df900",
    "#521b92",
    "#005392",
    "#008e00",
    "#941100",
    "#ff2f92",
    "#9437ff",
    "#0096ff",
    "#ff7d78",
    "#7980ff",
    "#72fcd5",
    "black",
    "#ff9300",
    "#919191",
    "#941651",
    "#8df900",
    "#521b92",
    "#005392",
    "#008e00",
    "#941100",
    "#ff2f92",
    "#9437ff",
    "#0096ff",
    "#ff7d78",
    "#7980ff",
    "#72fcd5",
]
c += c + c

# add links for tags
for _index, _row in taglinks.iterrows():
    cb = _row["checkbox"]
    # print(checknames[cb],c[cb],cb)
    net_graph.add_edge(
        _row["idsource"],
        _row["idtarget"],
        checkbox=cb,
        edge_color=c[cb],
        edge_width=0.0001,
        myweight=standard,
        percorso="none",
        line_alpha=0.4,
    )


# ================================ network layout

width = 600
height = 600

graph_plot = Plot(
    width=width, height=height, x_range=Range1d(-1.1, 1.1), y_range=Range1d(-1.1, 1.1)
)


# spring = nx.spring_layout(net_graph,weight="myweight")
# nx.draw_networkx_edges(net_graph,pos=nx.spring_layout(net_graph))
# nx.draw(net_graph)
# pl.show()
# set node degrees
degrees = nx.degree(net_graph)

node_sizes = []
for node in net_graph.nodes:
    node_size = 5 + (distinct_cats[node]) ** 1.5
    net_graph.nodes[node]["size"] = node_size
    net_graph.nodes[node]["fill_alpha"] = 0.5
    node_sizes.append(node_size)

graph_setup = from_networkx(
    net_graph, nx.spring_layout, scale=3, center=(0, 0), weight="myweight"
)
# graph_setup = from_networkx(net_graph, nx.kamada_kawai_layout,pos=spring)

graph_setup.node_renderer.glyph = Circle(
    size="size",
    fill_color="color",
    fill_alpha="fill_alpha",
    line_alpha=0.0,
)
graph_setup.edge_renderer.glyph = MultiLine(
    line_color="edge_color", line_alpha="line_alpha", line_width="edge_width"
)

#  add node category at centroid
positions = np.array(
    list(graph_setup.layout_provider.graph_layout.values())
)  # assume the dict is ordered!
categories = np.array(list(node_category.values()))  # assume the dict is ordered!
# print("shape of positions", positions.shape)
# print("shape of categories", categories.shape)

# =========== overlayed circles
x, y, r, c = [], [], [], []
for cat in unique_cat:
    # print(cat, categories)
    # get all nodes of a certain category
    group = positions[categories == cat]
    com = group.mean(axis=0)
    x.append(com[0])
    y.append(com[1])
    r.append(0.1 + group.ptp(axis=0).max() / 2)
    # print("ptp",group.ptp(axis=0), group)
# print(x)
textsource = ColumnDataSource(
    dict(comx=x, comy=y, text=unique_cat, color=list(catcolors.values()))
)
# print(list(catcolors.values()))
circlesource = ColumnDataSource(dict(x=x, y=y, sizes=r, color=list(catcolors.values())))

graph_plot.add_glyph(
    circlesource,
    Circle(
        x="x",
        y="y",
        radius="sizes",
        fill_color="color",
        fill_alpha=0.01,
        line_alpha=0.0,
    ),
)
background_text = Text(
    x="comx",
    y="comy",
    text="text",
    angle=0.0,
    text_color="color",
    text_alpha=1.0,
    text_font_size={"value": "14px"},
    text_font_style="italic",
)
graph_plot.add_glyph(textsource, background_text)

# highlight circles for nodes (to be filled by search)

highlight = ColumnDataSource(
    dict(
        x=positions[:, 0],
        y=positions[:, 1],
        radius=[0.03 for k in range(positions.shape[0])],
        line=["#ff2600" for k in range(positions.shape[0])],
        line_alpha=[0 for i in range(positions.shape[0])],
        fill_alpha=[0 for i in range(positions.shape[0])],
    )
)


graph_plot.add_glyph(
    highlight,
    Circle(
        x="x",
        y="y",
        radius="radius",
        fill_color="red",
        fill_alpha="fill_alpha",
        line_alpha="line_alpha",
        line_color="line",
        line_width=2,
    ),
)


node_hover_tool = HoverTool(
    renderers=[graph_setup],
    tooltips=[
        ("Titolo", "@title"),
        ("Autore", "@author"),
        ("ISBN", "@isbn"),
        ("Categoria", "@category"),
        ("Commenti", "@comment"),
        ("Catalogo", "@catalog"),
    ],
    show_arrow=False,
)
graph_plot.add_tools(ResetTool())
graph_plot.add_tools(node_hover_tool)
graph_plot.add_tools(WheelZoomTool())
graph_plot.toolbar.active_scroll = graph_plot.select_one(WheelZoomTool)

graph_plot.renderers.append(graph_setup)


source = graph_setup.edge_renderer.data_source

# print((graph_setup.edge_renderer.data_source.data['checkbox']))


title = Div(
    text="""<h1> Biblioteca CNR-Scienceground</h1>
    <ul>
    <li>
    Interfaccia grafica alla libreria condivisa Scienceground-CNR su Zotero <br/> <a href="https://www.zotero.org/groups/2536502/scienceground-cnr/library" target="_blank"> https://www.zotero.org/groups/2536502/scienceground-cnr/library</a> </i>
<li>Maggiori informazioni <a href="https://gitlab.com/extemporanea/biblio-cnr-scienceground"> qui</a></li>
</ul>"""
)

buttons = CheckboxButtonGroup(labels=["aree", "solo grandi nodi"], active=[0, 0])
buttons.js_on_event(
    "button_click",
    CustomJS(
        args=dict(
            btn=buttons,
            graph_plot=graph_plot,
            degrees=list(dict(degrees).values()),
            graph_setup=graph_setup,
        ),
        code=Path("js/buttons.js").read_text(),
    ),
)

explaintag = Div(
    text="Ogni titolo ha multipli tag associati. Spuntando le caselle, si possono identificare i titoli che condividono gli stessi tag. Titoli con più tag distinti hanno cerchi più grandi."
)
abstracts = Div(width=200, height=100, margin=(100, 0, 0, 0))


OPTIONS = list(percorsi.keys())
# print(OPTIONS)

multi_choice = MultiChoice(value=[], options=OPTIONS, height_policy="fit")
multi_choice.js_on_change(
    "value",
    CustomJS(
        args=dict(graph_setup=graph_setup, div=abstracts, percorsi=percorsi),
        code=Path("js/multi_choice.js").read_text(),
    ),
)

checkboxes = CheckboxGroup(labels=checknames, active=[], align="start")
checkboxes.js_on_change(
    "active",
    CustomJS(
        args=dict(graph_setup=graph_setup), code=Path("js/checkboxes.js").read_text()
    ),
)

# search
overview = Div(text="")
search = Select(value="--", options=["--"] + list(nodes["title"]))
search.js_on_change(
    "value",
    CustomJS(
        args=dict(
            graph_plot=graph_plot,
            titles=list(nodes["title"]),
            author=list(nodes["author"]),
            isbn=list(nodes["isbn"]),
            catalog=list(nodes["catalog"]),
            abstract=list(nodes["abstract"]),
            div=overview,
        ),
        code=Path("js/search.js").read_text(),
    ),
)
text_input = TextInput(placeholder="Inserisci parola presente nel titolo")
text_input.js_on_change(
    "value",
    CustomJS(
        args=dict(
            graph_plot=graph_plot,
            titles=list(nodes["title"]),
            titlelow=[n.lower() for n in list(nodes["title"])],
            s=search,
        ),
        code=Path("js/text_input.js").read_text(),
    ),
)

# ======== export to svg
# from selenium import webdriver
# from webdriver_manager.chrome import ChromeDriverManager
# driver = webdriver.Chrome(ChromeDriverManager().install())
# export_svg(  graph_plot,filename="nuvola.svg", webdriver=driver, width=2000, height=2000)

# ========= constructing panels
# tabs
tab2 = TabPanel(child=column(explaintag, checkboxes), title="Tag")
tab1 = TabPanel(child=column(multi_choice, abstracts), title="Percorsi")
tab3 = TabPanel(child=column(text_input, search, overview), title="Cerca")

layout = row(
    column(title, buttons, graph_plot, align=("end", "start")),
    Tabs(
        tabs=[tab1, tab2, tab3],
        margin=(
            50,
            0,
            0,
            0,
        ),
    ),
)
output_file(
    "../public/index.html", title="Biblioteca CNR-Scienceground"
)  # ../percorsi/automatici/html/networkx_graph.html")
# show(layout, template=template)
save(
    layout,
    "../public/index.html",
    title="Biblioteca CNR-Scienceground",
    template=template,
)
