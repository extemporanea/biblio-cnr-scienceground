# Codici per l'explorazione automatica della libreria

## Per analizzare i tag

Eseguire

`python ottienistat.py`

## Per produrre un grafo Bokeh

Eseguire i dure comandi seguenti:
1. `python links.py`
2. `python creanet.py`


## Per produrre una bozza Markdown di percorsi fra due tag

Eseguire

`python creapercorsi.py`
