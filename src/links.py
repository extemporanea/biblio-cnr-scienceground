import pandas as pd
import numpy as np
import myio

from pyzotero import zotero



library_id = '2536502'

api_key = '448IcF6k31mfLAEa7ywdsqaC'

library_type = 'group'

zot = zotero.Zotero(library_id, library_type, api_key)

items = zot.everything(zot.top())
# items = zot.top(limit=5)
print("Len items",len(items))
library = myio.read_library(items)
# print(library)

print("Number of items", len(list(library.keys())))

nodes = open("data/nodes.csv",'w')
with open("data/links.csv", 'w') as fout:
	nodes.write("id|title|author|isbn|catalog|abstract|\n")
	print("idsource|idtarget|source|target|edge_name",file=fout)
	for i,ti in enumerate(library.keys()):
		# print(ti)
		authors	= library[ti]['authors']
		isbn = library[ti]['ISBN']
		catalog	= library[ti]['catalog']
		abstract	= library[ti]['abstract']
		nodes.write(f"{i}|{ti.replace('|','-')}|\"{authors}\"|{isbn}|{catalog}|{abstract}|\n")

		# print(i, ti)
		tagsi = set(library[ti]['tags'])
		for j,tj in enumerate(library.keys()):
			if i !=j:
				tagsj = set(library[tj]['tags'])
				for k in tagsi:
					if k in tagsj:
				# intersection = tagsi.intersection(tagsj)
				# if len(intersection)>0:
					# for k in intersection:
						fout.write(f"{i}|{j}|{ti.replace('|','-')}|{tj.replace('|','-')}|{k}\n")
