import pandas as pd
from pyzotero import zotero
import numpy as np
import collections
import itertools
import myio


eXtemporanea = ['fturci','mariellen','elenalma','sdalci','michela.bersia','palful']

library_id = '2536502'

api_key = '448IcF6k31mfLAEa7ywdsqaC'

library_type = 'group'

zot = zotero.Zotero(library_id, library_type, api_key)
# print(inspect(zot.items))
items = zot.everything(zot.top())
# items = zot.top(limit=1)

count_eXtemp = 0
count_not_eXtemp = 0 
names = []
for i,item in enumerate(items):
    name = item['meta']['createdByUser']['username']
    names.append(name)
    if name in eXtemporanea:
        count_eXtemp += 1
    else:
        count_not_eXtemp += 1

print("Titoli aggiunti da membri di eXtemporanea", count_eXtemp)
print("Titoli aggiunti da membri del CNR", count_not_eXtemp)

counter = collections.Counter(names)

print(counter)