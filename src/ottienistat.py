import pandas as pd
from pyzotero import zotero
import numpy as np
import collections
import itertools
import myio


library_id = '2536502'

api_key = '448IcF6k31mfLAEa7ywdsqaC'

library_type = 'group'

zot = zotero.Zotero(library_id, library_type, api_key)
# print(inspect(zot.items))
items = zot.everything(zot.top())
# items = zot.top(limit=5)
print("The number of elements is", zot.count_items(), "and the list length is", len(items))

library = myio.read_library(items)

tags_count = []
tags_names = []
for title,content in library.items():
	print("Book",title)
	print(content['tags'])
	tags_count.append(len(content['tags']))

	tags_names.append(content['tags'])

tags_names = np.concatenate(tags_names)
print()
print ("Counts", np.bincount(tags_count))
counter = collections.Counter(tags_names)
print ("Number of tags", len(list(counter.keys())))
# print(counter)
for element in counter.most_common():
    print(element[0],'\t', element[1])