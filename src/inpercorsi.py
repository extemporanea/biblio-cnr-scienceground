class Reader:
    """Reads list of books from a Markdown file"""
    def __init__(self,filename):
        self.filename= filename

        self.percorsi = {}

        self.read()

    def read(self):
        with open(self.filename) as fin:
            for line in fin.readlines():
                # print(line)


                if line[0]=="#":
                    name = line.split("#")[1].strip()

                    # print("Nuovo percorso:", name)
                    self.percorsi[name] = {}
                    self.percorsi[name]['list'] = []

                if line[0]=="%":
                    self.percorsi[name]["abstract"]=line.split("%")[1].strip()
                if line[0]=="-":
                    # split title from comment
                    split = line.split(":")
                    item = split[0].split("-")[1].strip()
                    comment = split[1].strip()
                    self.percorsi[name]['list'].append({"title":item,"comment":comment})


# R = Reader("data/percorsi.md")
# print (R.percorsi)
