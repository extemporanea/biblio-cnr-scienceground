# Percorsi bibliografici

1. **Usi e abusi della statistica**
Mark Twain attribuiva (erroneamente) al primo ministro inglese Disraeli il motto "Lies, damned lies, and statistics." L'uso distorto della statistica è frequente nella vita di tutti i giorni, nei _mass-media_, nella stampa. Tuttavia anche la ricerca scientifica è soggetta a distorsioni spesso implicite e non intenzionali, che possono alterare significativamente il valore conoscitivo di una ricerca o di un campo di ricerca.

    - _Statistics Done Wrong_ è un ottimo libretto sull'uso e abuso della statistica nelle scienze, al contempo approfondito e pedagogico. A nostro avviso dovrebbe essere letto da qualsiasi studente universitario di materie scientifiche.
    - _Gravity's Ghost and Big Dog_ è un resoconto di come la comunità che ha scoperto le onde gravitazionali si è data seri criteri statistici di autorevolezza. In particolare il capitolo sull'inferenza bayesiana, spiegata da un sociologo (Harry Collins, uno dei grandi interpreti del contesto sociale della scienza ai nostri tempi), è molto profondo.
    - _Bad Science_ è una buona introduzione pop a certe fallacie statistiche nell'interpretazione di dati.
    - _Spurious Correlations_ è un simpatico _divertissement_  che mette a nudo il _confirmation bias_.
    - _How to Lie with Statistics_ è un classico, abbastanza superficiale e parzialmente infondato, sull'uso arbitrario della statistica da parte della politica. Duff ha poi usato queste stesse tecniche l'industria del tabacco contro l'accusa del danno ai polmoni delle sigarette... Lo includiamo per completezza del percorso storico.
    - _Crime Statistics in the News_ analizza l'uso le distorsioni dei numeri che avvengono riportando la cronaca nera nei giornali
    - _Reckoning with Risk_ è un classico della psicologia comportamentale e dei tranelli che logica e statistica pongono a quell'intreccio di ragionamento e istinto umano.
    - _The Manga Guide to Statistics_: vabbeh!
    - _The Visual Display of Quantitative Information_ di E Tufte, è un classico su come transmettere l'informazione scientifica in grafici, tabelle e mappe in un modo che se ne limitino le deformazioni e ci si concentri sull'essenziale.


1. **Storia e storie**
 In questo percorso raccogliamo testi che affrontano la prospettiva storica del _fare scienza_. Ritroviamo qui saggi che tracciano i cammini di idee accantonate, diari personali, biografie entusiaste, approcci comparativi fra i problemi del presente e quelli degli antichi. La raccolta è un viaggio nella scienza del passato, con un occhio alla scienza del futuro.
 
    - _La Scienza Che Fu_, di F Barreca,  un saggio sui destini delle idee seducenti (ma abbandonate) che hanno costellato il cammino della Fisica e della Chimica, come l'etere, il flogisto o gli atomi-vortici. Idee che han fatto da guida, talora portando su false piste, talaltra venendo riscoperte sotto nuova luce.
    
    - _Viaggio di un professore tedesco all’Eldorado_  è il diario di Ludwig Boltzmann, il padre della meccanica statistica durante il suo viaggio oltreoceano alla scoperta dell'America (accademica) dei primi del Novecento. Boltzmann e le sue idee ebbero vita dura per farsi largo fra vecchie misconcezioni. Nel diario, però, troviamo un libriccino leggero, in cui si parla di oceano, natura e (occasionalmente) birra, e che ci sorprende se letto alla luce della tragica depressione che travolse il fisico irreparabilmente poco tempo dopo.
    
    - _L’invenzione della natura_ di A Wulf  ci porta dal turista (e sfortunato) Boltzmann all'esploratore affermato e uomo di mondo Alexander von Humboldt. In un percorso ben documentato - sebbene con qualche passaggio agiografico - riscopriamo una visione unitaria dei fenomeni fisici, chimici e biologici e gli andirivieni fra sensibilità pre-romantica e le scienze naturali. 
    - _The Scientific Method_ di H Cowles rivisita la nozione di metodo scientifico e la sua evoluzione, appoggiandosi sulla visione di John Dewey all'educazione progressiva e i suoi legami con le teorie di Darwin. In una prospettiva storica, il libro rilegge la costruzione del metodo scientifico come una spiegazione (a posteriori) del progresso tecnologico esperito dagli uomini dell'Ottocento e esplora i legami di questa nozione con le scienze umane.
    - _Il dottor Semmelweis_ di Céline, ovvero perché lavarsi le mani? Un avvincente giallo scientifico in cui il nostro eroe Semmelweis va alla ricerca causale della differente mortalità delle partorienti in due reparti di un centro ostetrico, il tutto ambientato nella Vienna di metà XIX secolo.
       
    - _Deadly Companions_ di D Crawford ci racconta del rapporto simbiotico fra microbi e sviluppo delle civiltà. Ci propone un percorso introduttivo fa malattie che consideriamo recondite - come la peste bubbonica - e le civiltà antiche e moderne in cui ancora queste albergano. 
    
    - _Environmental Problems of the Greeks and Romans_ di J D Hughes mette in prospettiva la criticità ambientale del presente nel contesto storico di una delle culle della civiltà: il Mediterraneo. Deforestazione, sfruttamento delle risorse minerarie e idriche appaiono allora non come cifre della modernità, ma come costanti dello sviluppo della civiltà umana, con le sue irrisolte contraddizioni.
    - _The Dialectical Biologist_ di R Levins e R Lewontin è una collezione di saggi a vasto spettro su scienza, medicina e storia da un punto di vista materialista, in contrasto con approcci riduzionisti al sapere.
    - _Che cos’è la scienza_ una ricostruzione storica classica che, partendo da Anassimandro di Mileto, ricostruisce il percorso scientifico attraverso le sue rivoluzioni e le sue relazioni con altre forme di conoscenza.
    - _Storia Popolare della Scienza_ cerca di mettere in luce il ruolo delle conoscenze popolari e delle persone "minori" nella realizzazione di grandi scoperte tecniche e paradigmi concettuali, seguendo una tradizione storiografica (per esempio Howard Zinn, _A People's History of the United States_) che sistematicamente cerca di superare le narrazioni storiche incentrate sugli eroi e sulle grandi imprese.
    - _A Thousand Years of Nonlinear History_ è esattamente ortogonale a quest'ultimo: è il tentativo di raccontare la grande storia dei fenomeni collettivi attraverso i fenomeni macrofisici e tecnologici che hanno interessato le risorse naturali, influenzando le comunità umane come fossero materia soggetta a dinamiche altamente non lineari.
    - _Leviathan and the Air-Pump_ è un resoconto storico-filosofico della diatriba tra Hobbes e Boyle e della separazione netta, ma speculare, che ne è conseguita tra leggi di natura e leggi umane. 



3. **Paradigmi evolutivi**
    - _Evolution - The extended Synthesis_ è una corposa monografia che discute i più recenti sviluppi del paradigma teorico evolutivo a fronte delle novità offerte dalle tecniche sperimentali. Un vero e proprio tentativo di _sintesi_ - qui intesa come integrazione - tra categorie e discipline (in primis biologiche e filosofiche).
    - _The new foundations of Evolution. On the tree of Life_ Proposta meno tecnica della precedente in cui l'autore analizza i fondamenti della teoria evoluzionistica "classica" alla luce dei progressi della microbiologia; una disciplina relativamente giovane e intrinsecamente anti-riduzionista se confrontata al paradigma genetico.
    -  _The dialectical Biologist_ Una sorta di manifesto metodologico dei due biologi marxisti i quali alternano capitoli dedicati a teorie biologiche con analisi storiche e politiche mostrando come ogni aspetto della vita sociale (scienza inclusa) sia interconnesso. << Scientists, whether they realize it or not, always choose sides >>.
   
   
    - _Limits of the self - Immunology and biological identity_ Incorporare i microbi nelle categorie fondamentali della biologia moderna non mette in discussione solo la teoria dell’evoluzione ma anche la stessa definizione di identità biologica. Il filosofo Pradeu, che ama definirsi filosofo _nella_ biologia, propone una nuova teoria “del sé” di stampo immunologico.
    - _Tempi storici, tempi biologici_ Pubblicato per la prima volta nel 1984 rimane un saggio attualissimo nel denunciare la crisi del nostro rapporto con la natura.
    - _Galapagos_  La coscienza ecologica già raccontata da Tiezzi entra, un anno più tardi, anche nel mondo della fantascienza in salsa distopica.

4. **Metodi scientifici**

    - _The scientific method_ Rivisitazione storica, e quindi umanistica,  della nozione di metodo scientifico.
    - _Beyond Nature and Culture_ L'approccio antropologico prospettivista  è un esercizio prezioso per scongiurare l'autoincensimento assolutista  (e spesso strumentale) della scienza. Proponiamo allora due testi puramente antropologici che hanno segnato profondamente la disciplina in anni recenti.
    - _Prospettivismo cosmologico in Amazzonia e altrove_ L'impossibilità di distringuere umano e non-umano è qui portata all'estremo da Viveiros de Castro che mette in discussione la nozione stessa di soggettività alla base della conoscenza.
    - _Laboratory life_ Con Latour arriviamo al giro di boa e _rientriamo dall'ingresso sul retro_. Un antropologo visita un laboratorio e ne studia la comunità che lo abita.
    - _Gravity's Ghost and Big Dog_ è un resoconto dei riti, miti, pratiche della comunità LIGO-Virgo, osservata dall'interno dal sociologo Harry Collins per oltre quarant'anni.
    - _Lost in math. How beauty leads physics astray_ Sabine Hossenfelder si sente ad una svolta della sua carriera da fisico teorico. Parte alla raccolta delle storie e delle testimonianze di amici e colleghi per verificare lo stato di salute della sua amata disciplina.
    - _I fisici_ La chiosa distopica è qui affidata al teatro di Durrenmatt.  "Tutto ciò che è pensabile, una volta o l’altra verrà pensato".

5. **Microbi**

    - _Selected papers in molecular biology_: una selezione di articoli in inglese e francese di Jacques Monod in cui si mostra la nascita  della biologia molecolare a partire dalla microbiologia, e quindi il passaggio  da uno studio macroscopico e osservativo dei microbi alla realizzazione del loro potenziale come organismi utili tecnicamente al lavoro degli scienziati. 
    - _The pasteurization of France_: la scienza non è un racconto avulso dagli interessi socio-culturali del contesto storico in cui si sviluppa. Il successo di Pasteur come fondatore della microbiologia medica ne è una dimostrazione. 
    - _Mosquito trails_: i microbi possono causare malattie e nel farlo interagiscono con l'insieme delle credenze sociali e culturali della popolazione che colpiscono. La febbre dengue è solo un caso studio di questo "entanglement".
    - _Microbes and other shaman beings_: non c'è un solo modo di fare scienza, non esistono definizioni assolute: il voler imporre un punto di vista si chiama colonialismo. L'antropologo e biologo César Giraldo Herrera racconta i microbi come pensati dalla tradizione sciamanica sudamericana. 
    - _How to survive a plague_: una malattia infettiva non è descritta solo dai meccanismi biologici e molecolari di contagio e diffusione, ma anche dall'interazione col vissuto personale e culturale di chi la contrae. Questa interrelazione genera la necessità di nuovi modi di fare ricerca e trovare soluzioni, come in questo racconto di lotta e ricerca militante su AIDS e HIV negli Stati Uniti.  
    - _The drugs don't work_: a volte i limiti della scienza e della ricerca sono dati dallo stesso sfruttamento che si fa dei loro prodotti. E' il caso degli antibiotici, di cui l'uso smodato e spesso non necessario ha reso inevitabilmente inefficaci gli effetti, compromettendo pericolosamente la funzionalità della maggior parte delle pratiche mediche. 
 

7. **Scienza, Guerra, Società**
Questo percorso, sviluppato a partire da una serie di incontri organizzati nel 2020 in collaborazione con il gruppo ADI a Palazzo Ducale a Genova, nasce dall'esigenza da un lato di discutere il sempre maggiore coinvolgimento della scienza e della ricerca nello sviluppo di applicazioni militari; dall'altro di analizzare questo legame tra scienza e guerra anche da un punto di vista epistemologico, a partire dall'idea di controllo e sfruttamento della natura implicita in molta della ricerca scientifica. 

    - _Drone theory_: l'uso dei droni nella guerra contemporanea non è solo un avanzamento tecnologico, ma l'esito dei processi di spersonalizzazione e allontanamento del 'nemico' che rendono l'atto bellico un processo che può essere svolto dietro ad un computer, senza alcun coinvolgimento emotivo di chi decide di sparare ed uccidere  
    - _War against the people_: L'antropologo Jeff Halper mostra come il concetto di guerra stia cambiando verso quello di una "guerra securocratica", basata su tecnologie del controllo piuttosto che armamenti pesanti. Paesi come Israele e la Cina sono capofila nell'avanzamento di queste tecnologie, al cui sviluppo cui partecipano anche ricercatori in università pubbliche e private.  
    -  _War in the age of Intelligent Machines_: il cambiamento nelle modalità e nelle tecnologie usate nel fare guerra implica un cambiamento nel rapporto tra uomo e quelle stesse tecnologie. 
    - _Che cos'è reale?_: a partire dalla storia del fisico Ettore Majorana, Agamben si interroga sulle relazioni stabilite dagli scienziati con la natura e dal tipo di rapporto che la 'nuova' fisica quantistica potrebbe invece iniziare. 
    - _Cuba: medicina, scienza e rivoluzione_: lo sviluppo della ricerca scientifica a Cuba dopo la rivoluzione può essere il paradigma di uno sviluppo scientifico rivolto al benessere di una comunità. 

8. **Tra salute e malattia**
    - _Il dottor Semmelweis_ ovvero perché lavarsi le mani? Un avvincente giallo scientifico in cui il nostro eroe Semmelweis va alla ricerca causale della differente mortalità delle partorienti in due reparti di un centro ostetrico, il tutto ambientato nella Vienna di metà XIX secolo.

    - _Cuba: medicina, scienza e rivoluzione, 1959-2014: perché il servizio sanitario e la scienza sono all'avanguardia_ è un’interessante riflessione sul tipo di ricerca scientifica e assistenza medica che si può realizzare in un contesto sociale ispirato al marxismo, scenario nel quale il loro buon successo viene considerato imprescindibile per la tenuta stessa della rivoluzione. Che relazioni ci sono dunque tra successo scientifico e modello economico?

    - _Un nuovo mo(n)do di fare salute_ parte dall’analisi critica dei sistemi sanitari per poi portare avanti le proposte costruttive della Rete Sostenibilità e Salute: si auspica una riforma strutturale della sanità al fine di pagare per la salute, non la malattia. 

    - _Medici senza camice. Pazienti senza pigiama. Socioanalisi narrativa dell'istituzione medica_ è un report attento costruito da un tavolo di lavoro composto da studenti,specializzandi e operatori nell'ambito della salute. Tramite una serie di testimonianze dirette si tenta di realizzare una narrazione critica e puntuale delle gerarchie di potere interne all'istituzione medica e al percorso di formazione in medicina. È un'esortazione a non farsi complici passivi e un invito a uscire dalla solitudine per creare nuovi immaginari di cura condivisi. Assolutamente da leggere, soprattutto se studenti delle professioni sanitarie o prima della scelta dell'università. 

    - _Etnopsichiatria. Sofferenza mentale e alterità fra storia, dominio e cultura_ è un denso manuale di approfondimento del disagio mentale in società non occidentali che, partendo dalle radici storiche in epoca coloniale, ci porta poi a riflettere sulle differenti rappresentazioni della malattia in società Altre, il dialogo con i guaritori e le terapie tradizionali, ma anche la critica della psichiatria occidentale e la clinica della migrazione.

    - _Primo: non curare chi è normale. Contro l'invenzione delle malattie_.  Allen Frances è stato il capofila dell'equipe che ha messo a punto la terza e la quarta edizione del DSM (_Manuale diagnostico e statistico dei disturbi mentali_). Nonostante un lavoro di accurata documentazione scientifica, dopo tanti anni torna a interrogarsi sulle pressioni implicite ed esplicite che hanno condizionato la definizione di malattia psichica, facendo un _mea culpa_ su due sindromi che lui stesso ha contribuito a rendere "popolari" e sovradiagnosticate: i disturbi dello spettro dell'autismo e il disturbo da deficit dell'attenzione e iperattività.

    - _Antropologia medica. Saperi, pratiche e politiche del corpo_ è un testo introduttivo accessibile a tutti, molto utile nel fornire elementi di base per ripensare concetti complessi e talvolta ambigui come salute e la malattia, relazione medico paziente, esperienza del dolore e strategie di cura.

    - _Microbes and other shamanic beings_ è un'indagine sulle conoscenze microbiologiche degli sciamani amerindi, e su come le moderne concezioni scientifiche del contagio discendono dai saperi importati, assieme alla sifilide, dagli esploratori colombiani dalle Indie











