# Lettera di motivazioni

> «A Ersilia, per stabilire i rapporti che reggono la vita della città, gli abitanti tendono dei fili tra gli spigoli delle case, bianchi o neri o grigi o bianco-e-neri a seconda se segnano relazioni di parentela, scambio, autorità, rappresentanza. Quando i fili sono tanti che non ci si può più passare in mezzo, gli abitanti vanno via: le case vengono smontate; restano solo i fili e i sostegni dei fili»
> 
> Italo Calvino, _Le Città Invisibili_



Care e cari ricercatori, ricercatrici, e personale del CNR di Bologna,

ci chiamiamo Maria Elena Antinori, Sara Dal Cengio, Matteo Polettini, Elena Alma Rastello, e Francesco Turci. Insieme partecipiamo a iniziative che ci piace dire di "conversazione" scientifica insieme a un [gruppo variegato e fluido](https://scienceground.it/).

Sappiamo che Silvana ha già dato le coordinate tecnice del progetto. Con questa seconda lettera vorremmo dare forma all'"idea", che speriamo possa definirsi con il vostro contributo.

---

Spesso come comunità abbiamo sentito la necessità di scambiarci dei libri. Occasionalmente abbiamo coordinato percorsi liberi di lettura condivisa. Negli anni questo ci ha portato a collezionare una biblioteca ragguardavole, sparsa però sulle mensole delle nostre librerie di casa.

Da qualche anno pezzi di questa biblioteca ci seguono, in assetto variabile, in alcune occasioni pubbliche in cui siamo coinvolti. Quando l'allestimento favoriva l'incontro, da parte dei partecipanti c'è stata molta curiosità rispetto alla scelta dei titoli proposti, portandoci spesso a intrattenere conversazioni approfondite, come quella con Silvana, Ornella, Stefania, Debora e Gabriela,  a [Festivaletteratura 2019](http://biblioteca.bo.cnr.it/index.php/it/archivio-eventi/altri-eventi/item/341-la-biblioteca-d-area-partecipa-al-festival-della-letteratura-di-mantova-2019).

In un mondo della comunicazione scientifica (o, come corre dire, dell'_outreach_ e del _public engagement_) che si va via via affollando, crediamo che invece di condurrre le persone per mano "spiegando le cose come stanno" serva fornire loro mappe per renderle libere di orientarsi in territori inesplorati. _HC SVNT DRACONES_ indicava una carta medievale ai bordi delle terre note. Come ricercatrici e ricercatori possiamo demistificare i dragoni e, al contempo, rendere la rappresentazione più complessa e articolata. Per questo abbiamo bisogno di **bibliografie** come mappe concettuali per orientarci in un mondo che pullula di _big data_ ma che a volte manca di narrazioni condivise.

Ecco perché la nostra biblioteca disgregata ospita una varietà di generi:

- qualche monografia tecnica;

- alcuni classici della divulgazione scientifica;

- poche recenti novità editoriali (se davvero valide);

- romanzi, narrativa, giornalismo d'inchiesta, fumetti, manga, riviste d'arte, fanzine, manuali per l'uso, etc.;

- libri che non c'entrano nulla, finiti "per caso" nel calderone;

- monografie accademiche di stampo socio/filo/antropo/semio/etno/polito/etcetera-logico sul discorso scientifico e il suo impatto nella società.

Quest'ultima fattispecie è la punta della lancia, il macete con cui ci facciamo largo nella giungla ignota, i cui scorci lasciano intravedere ibridazioni impensate di naturale e artificiale. A volte abbiamo guardato ai saperi umanistici con supponenza: sono però le conoscenze di chi ci ha osservato _come comunità_, e pertanto dobbiamo famigliarizzare con questi testi se vogliamo "comprenderci".

---

Tuttavia le bibliografie non bastano. Servono le **biblioteche**. Non come granai in cui ammassare riserve di conoscenza, ma piuttosto come corti dinamiche.

Non siamo dei feticisti dei libri. Se necessario per sopravvivere, con le pagine di un libro ci si può attizzare un fuoco. Il libro cartaceo ci ricorda però che quello che produciamo non è pura informazione astratta, ma -- proprio come i _paper_ a cui siete dediti (che non sono solo "carta") -- comporta mobilitazione di risorse materiali e immateriali. È allora un dramma che, lontano da sguardi indiscreti, quotidianamente avvengano simbolici _bücherverbrennungen_ per smaltire le eccedenze di un'industria editoriale in confusione, che cerca di modellare il gusto di comunità di lettori sempre più consumatori e sempre meno comunità. Ed è un brutto segnale che un'editoria accademica "in cerca d'autore" senta la necessità di creare sempre nuove riviste, via via più "autorevoli", a volte fintamente _open access_, senza fornire nuovi strumenti critici. Eccoci così vagare «alla ricerca dei pochi libri di senso compiuto» in una biblioteca di Babele composta, parafrasando Borges, da combinazioni casuali di fattoidi scientifici non accertati, e dalla grammatica degli _abstract_, delle bibliografie e degli _acknowledgements_.

Per questo quando la Biblioteca d'Area del CNR di Bologna ci ha invitato a proporre una scelta di titoli per fare una prima acquisizione di volumi, abbiamo pensato che a differenza di una bibliografia istituzionale, esaustiva e universale (e quindi un po' anonima), avremmo potuto sperimentare nuovi modi di selezionare e organizzare il materiale, dando vita a una collezione di libri che si struttura dinamicamente tramite gli stessi percorsi di lettura. Ci siamo allora impegnati a mettere a punto alcuni semplici strumenti _free and open source_ per condividere scelte e pensieri, in linea con la lunga e ammirevole [esperienza in fatto di condivisione del sapere scientifico](https://nilde.bo.cnr.it/) della Biblioteca d'Area. Gli strumenti consentono di proporre l'acquisizione di libri, di proporre percorsi tematici, di aggiungere titoli ad alcuni percorsi già cominciati, o di commentare un libro presente in biblioteca.

I percorsi che assieme ai bibliotecari e alle bibliotecarie del CNR abbiamo curato per "dare il La" a questa iniziativa riguardano alcune grandi questioni che abbiamo affrontato negli ultimi anni, a partire dalle parole chiave che ogni anno scegliamo per indirizzare la nostra attività: in particolare i **bit** e il connubio/dissidio tra informazione e materia, i **dati** e il loro trattamento statistico tra usi e abusi, i **microbi** e la loro percezione e concezione, gli **ecosistemi** e l'integrazione dei metodi di ricerca che il loro studio comporta, l'**intelligenza artificiale** tra fatti, miti, e interessi, la questione di **genere** nelle scienze come indagine sugli aspetti discriminatori nell'accesso al mondo della produzione e fruizione della conoscenza, la **decolonizzazione** come restituzione di valore scientifico alle conoscenze indigene (specie degli indigeni che vivono in quelle particolari "riserve" che sono le università e gli enti di ricerca!). Abbiamo spesso scelto titoli che esistono solo in lingua originale (quasi sempre Inglese), o che comunque sono difficili da reperire nelle altre biblioteche bolognesi: non per snobismo, ma perché mettendole in circuito tramite l'OPAC speriamo di complementare l'offerta documentale della città di Bologna. Inoltre, gli strumenti consentono di farli dialogare con i più rilevanti articoli scientifici sull'argomento, creando connessioni tra due tipi di bibliografia che raramente si parlano pubblicamente.

L'obiettivo a breve termine è fare in modo che, quando sarà ancora popolabile, il bellissimo spazio in Via Pietro Gobetti possa animarsi di discussioni. A lungo termine, l'ambizione è quella di creare una nuova _autorevolezza_, nel senso letterale del termine. Per questo vi chiediamo di usare un po' della credibilità che come ricercatrici e ricercatori avete acquisito lavorando con metodo, per diventare _(co)autori_ di nuove vie attraverso quelle mappe che col vostro lavoro state aiutando a disegnare. Della vostra autorevolezza abbiamo tanto bisogno in tempi in cui rischiamo da un lato la sparizione di un metodo ormai antico -- quel connubio unico di ragione ed esperienza già invocato da [Sant'Agostino](http://www.youtube.com/watch?v=FM6GetH2C3M&t=28m0s) -- e dall'altro l'esercizio arbitrario dell'autorità.
 
 

Un caro saluto,

Elena Alma,  Francesco, Maria Elena, Matteo e Sara

per

E<span style="font-family:Courier,sans-serif;font-size:1.8em;">X</span>TEMPORANEA


[sito](https://scienceground.it/)
[telegram](https://t.me/extemporanea)
[newsletter](https://scienceground.us10.list-manage.com/subscribe/post?u=e63aec762a341d1273e04e347&id=967b9844d4) 
[podcast](https://www.spreaker.com/show/extemporanea-sciencesound)




















