Care Silvana e Ornella

sono Francesco Turci, mi sto occupando con il gruppo eXtemporanea del progetto bibliografico.

Abbiamo pensato di creare degli strumenti online con cui condividere con i ricercatori del CNR interessati alla possibilità di creare bibliografie e percorsi tematici. Partiamo noi con una proposta bibliografica di base e, se non dovessero esserci ulteriori aggiunte entro il termine, provvederemo a completare la scelta dei volumi da acquistare. Ciononostante, ci terremmo a coinvolgere i ricercatori del CNR, anche se i tempi sono stretti.

Qui sotto una breve descrizione degli strumenti, che sono intuitivi e che potete già inoltrare ai ricercatori. La settimana prossima poi vorremmo inviare loro una mail di intenti e qualche esempio di percorso ([qui](https://demo.codimd.org/s/H1nBhBMLP#) un draft, ancora incompleto: ogni suggerimento è benvenuto!). 

Abbiamo strutturato il progetto Biblioteca Scienceground-CNR su due gambe:

- da una parte c’è una gruppo condiviso su Zotero  all’indirizzo
	https://www.zotero.org/groups/2536502/scienceground-cnr/library

- dall’altra c’è un progetto su Gitlab per depositare i documenti in maniera collaborativa e tracciabile,
	https://gitlab.com/extemporanea1/biblio-cnr-scienceground

I passi e procedure per unirsi al gruppo di Zotero e modificare la libreria sono descritti nella pagina del progetto Gitlab. In estrema sintesi, è sufficiente:
1. iscriversi a Zotero
2. recarsi sulla pagina https://www.zotero.org/groups/2536502/scienceground-cnr/library e richiedere l’iscrizione al gruppo (o, in alternativa, contattare turci.francesco@gmail.com per l’iscrizione)
3. installare il client Zotero
4. aggiungere e editare i titoli della libreria condivisa.

Per una guida in italiano sull’uso di Zotero, si può fare riferimento al documento elaborato dall’Università di Bologna https://sba.unibo.it/it/almare/servizi-e-strumenti-almare/gestione-bibliografie

Abbiamo al momento poco meno di un centinaio di titoli organizzati in 37 tag (o parole chiave). L’idea è di cercare di mantenere un numero limitato di tag capaci di connettere tematiche diverse e che ci orientino nella costruzione di percorsi.

Come dimostrazione del tipo di relazioni che proponiamo fra questi testi, alleghiamo alcuni percorsi di lettura: questi connettono tematiche disparate e eteroclite. I percorsi non sono esaustivi (altri percorsi sono possibili e non tutti i libri sono ancora organizzati in percorsi); i percorsi non sono neanche esclusivi (due percorsi possono condividere lo stesso titolo e in questo modo intrecciarsi). A termine, queste relazioni fra titoli e parole chiave ci potrà permettere di costruire strumenti di esplorazione della libreria non-lineari, basati ad esempio sulle proprietà del network di referenze risultante.

In caso di bisogno di chiarimenti, contatta liberamente turci.francesco@gmail.com o altri della comunità di eXtemporanea.

Grazie

Francesco Turci, per eXtemporanea
